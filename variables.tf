variable "ami_id" {
  type        = string
  description = "Instance AMI"
}

variable "instance_name" {
  type        = string
  description = "Instance name"
}

variable "type" {
  type        = string
  description = "Instance type"
  default     = "t3.micro"
}

variable "key_name" {
  type        = string
  description = "Instance SSH key name"
  default     = null
}

variable "subnet_id" {
  type        = string
  description = "Subnet where the instance gets deployed"
}

variable "vpc_id" {
  type        = string
  description = "VPC where the instance gets deployed"
}

variable "associate_public_ip_address" {
  type        = bool
  description = "Whether to associate a public IP address with an instance in a VPC"
  default     = false
}

variable "userdata" {
  type        = string
  description = "Inline userdata - will be used in favour of any other userdata if present"
  default     = null
}

variable "nat_instance" {
  type        = bool
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs"
  default     = false
}