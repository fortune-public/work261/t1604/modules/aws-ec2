output "instance_id" {
  value       = aws_instance.this.id
  description = "Instance id"
}

output "sg_id" {
  value       = aws_security_group.this.id
  description = "Instance security group id"
}

output "role_id" {
  value       = aws_iam_role.this.id
  description = "Instance role id"
}

output "role_arn" {
  value       = aws_iam_role.this.arn
  description = "Instance role arn"
}

output "public_ip" {
  value       = aws_instance.this.public_ip
  description = "Instance public IP"
}

output "private_ip" {
  value       = aws_instance.this.private_ip
  description = "Instance private IP"
}
