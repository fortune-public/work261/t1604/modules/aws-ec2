resource "aws_instance" "this" {
  ami                         = var.ami_id
  instance_type               = var.type
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  iam_instance_profile        = aws_iam_instance_profile.this.id
  associate_public_ip_address = var.associate_public_ip_address
  user_data_base64            = local.base64_userdata
  security_groups             = [aws_security_group.this.id]
  source_dest_check           = var.nat_instance

  # root disk
  root_block_device {
    volume_size           = "25"
    volume_type           = "gp3"
    encrypted             = true
    delete_on_termination = true
  }

  tags = {
    Name = var.instance_name
  }

  lifecycle {
    ignore_changes = [security_groups]
  }
}

resource "aws_iam_role" "this" {
  name = "${var.instance_name}-ROLE"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  tags = {
    name = "${var.instance_name}-ROLE"
  }
}

resource "aws_iam_role_policy_attachment" "session_manager" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "cw_agent" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

resource "aws_iam_instance_profile" "this" {
  name = var.instance_name
  role = aws_iam_role.this.name
}

resource "aws_security_group" "this" {
  name   = var.instance_name
  vpc_id = var.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = var.instance_name
  }

  lifecycle {
    # Necessary if changing 'name' or 'name_prefix' properties.
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "icmp" {
  type              = "ingress"
  from_port         = 8
  to_port           = 0
  protocol          = "icmp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow ping"
  security_group_id = aws_security_group.this.id
}

locals {

  simple_userdata = <<-EOT
    #cloud-config
    package_update: true
    package_upgrade: true

    packages:
     - iptables-services

    runcmd:
      # https://superuser.com/questions/572172/what-are-reasons-to-disallow-icmp-on-my-server
      # https://www.udemy.com/course/linux-security-the-complete-iptables-firewall-guide/learn/lecture/11177454?start=75#overview
      # ping -i 0.1 target-ip
      # tcpdump icmp -n
     - systemctl enable iptables
     - systemctl start iptables
     - iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/sec --limit-burst 10 -j ACCEPT
     - iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
     - service iptables save
  EOT

  base64_userdata = try(base64encode(var.userdata), base64encode(local.simple_userdata)
  )
}