# aws-ec2

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.48.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.48.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.cw_agent](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.session_manager](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.icmp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | Instance AMI | `string` | n/a | yes |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Whether to associate a public IP address with an instance in a VPC | `bool` | `false` | no |
| <a name="input_instance_name"></a> [instance\_name](#input\_instance\_name) | Instance name | `string` | n/a | yes |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Instance SSH key name | `string` | `null` | no |
| <a name="input_nat_instance"></a> [nat\_instance](#input\_nat\_instance) | Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs | `bool` | `false` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | Subnet where the instance gets deployed | `string` | n/a | yes |
| <a name="input_type"></a> [type](#input\_type) | Instance type | `string` | `"t3.micro"` | no |
| <a name="input_userdata"></a> [userdata](#input\_userdata) | Inline userdata - will be used in favour of any other userdata if present | `string` | `null` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC where the instance gets deployed | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | Instance id |
| <a name="output_private_ip"></a> [private\_ip](#output\_private\_ip) | Instance private IP |
| <a name="output_public_ip"></a> [public\_ip](#output\_public\_ip) | Instance public IP |
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | Instance role arn |
| <a name="output_role_id"></a> [role\_id](#output\_role\_id) | Instance role id |
| <a name="output_sg_id"></a> [sg\_id](#output\_sg\_id) | Instance security group id |
<!-- END_TF_DOCS -->